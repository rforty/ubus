package facci.ronny_forty.ubus.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import facci.ronny_forty.ubus.Modelo.BusModel;
import facci.ronny_forty.ubus.R;

public class RecyclerBusAdapter extends  RecyclerView.Adapter<RecyclerBusAdapter.BusViewHolder>{

    private Context mContext;
    private int layoutResource;
    private ArrayList<BusModel> arrayListBuses;

    public RecyclerBusAdapter(Context mContext, int layoutResource, ArrayList<BusModel> arrayListBuses) {
        this.mContext = mContext;
        this.layoutResource = layoutResource;
        this.arrayListBuses = arrayListBuses;
    }

    @NonNull
    @Override
    public BusViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(mContext).inflate(layoutResource, viewGroup,false);
        return new BusViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusViewHolder busViewHolder, int position) {
        BusModel busModel = arrayListBuses.get(position);
        busViewHolder.mHorabus.setText(busModel.getHora());
        busViewHolder.mCodigobus.setText(busModel.getCodigo());
        busViewHolder.mAsientosbus.setText(String.valueOf(busModel.getAsientos()));
        busViewHolder.mPlacabus.setText(busModel.getPlaca());
        busViewHolder.mImagebus.setImageResource(R.drawable.ic_directions_bus_white_24dp);




    }



    @Override
    public int getItemCount() {

        if(arrayListBuses.size()>0){
            return arrayListBuses.size();
        }
        return 0;
    }




    public class BusViewHolder extends  RecyclerView.ViewHolder {

        TextView mHorabus, mCodigobus, mAsientosbus, mPlacabus;
        ImageView mImagebus;

        public BusViewHolder(@NonNull View itemView) {
            super(itemView);
            mHorabus     = itemView.findViewById(R.id.tv_rowhora);
            mCodigobus  = itemView.findViewById(R.id.tv_rowcodigo);
            mAsientosbus    =itemView.findViewById(R.id.tv_rowasientos);
            mPlacabus       =itemView.findViewById(R.id.tv_rowplaca);
            mImagebus      = itemView.findViewById(R.id.iv_rowbus);


        }

    }


}
