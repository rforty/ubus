package facci.ronny_forty.ubus.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import facci.ronny_forty.ubus.Modelo.BusModel;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.BusView.Reserva;

public class RecyclerBusAdapterUser extends  RecyclerView.Adapter<RecyclerBusAdapterUser.BusViewHolderUser>{

    private Context mContext;
    private int layoutResource;
    private ArrayList<BusModel> arrayListBusesUser;

    public RecyclerBusAdapterUser(Context mContext, int layoutResource, ArrayList<BusModel> arrayListBuses) {
        this.mContext = mContext;
        this.layoutResource = layoutResource;
        this.arrayListBusesUser = arrayListBuses;
    }

    @NonNull
    @Override
    public BusViewHolderUser onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(mContext).inflate(layoutResource, viewGroup,false);
        return new BusViewHolderUser(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusViewHolderUser busViewHolderUser, int position) {
        BusModel busModel = arrayListBusesUser.get(position);
        busViewHolderUser.mHorabus.setText(busModel.getHora());
        busViewHolderUser.mAsientosbus.setText(String.valueOf(busModel.getAsientos()));
        busViewHolderUser.mImagebus.setImageResource(R.drawable.n2);
        busViewHolderUser.mButtonReservar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, Reserva.class);
                //extras
                mContext.startActivity(intent);

            }
        });




    }



    @Override
    public int getItemCount() {

        if(arrayListBusesUser.size()>0){
            return arrayListBusesUser.size();
        }
        return 0;
    }




    public class BusViewHolderUser extends  RecyclerView.ViewHolder  {

        TextView mHorabus, mAsientosbus;
        pl.droidsonroids.gif.GifImageView mImagebus;
        Button mButtonReservar;// a

        public BusViewHolderUser(@NonNull View itemView) {
            super(itemView);

            //itemView.setOnClickListener(this);
            //mButtonReservar.setOnClickListener(this);
            mHorabus     = itemView.findViewById(R.id.tv_rowhora_user);
            mAsientosbus  = itemView.findViewById(R.id.tv_rowasientos_user);
            mImagebus      = itemView.findViewById(R.id.iv_rowbus_user);
            //a
            mButtonReservar = itemView.findViewById(R.id.btn_reservar_rowbus);

        }

      /*  @Override
        public void onClick(View v) {

        }
        */

    }


}