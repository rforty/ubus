package facci.ronny_forty.ubus.Presentador.AdminPresenter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facci.ronny_forty.ubus.Adaptadores.RecyclerBusAdapter;
import facci.ronny_forty.ubus.Modelo.BusModel;
import facci.ronny_forty.ubus.R;

public class PresenterAdmin {

    private Context mCOntext;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private RecyclerBusAdapter mAdapter;

    public PresenterAdmin(Context mCOntext, DatabaseReference mDatabase, FirebaseAuth mAuth) {
        this.mCOntext = mCOntext;
        this.mDatabase = mDatabase;
        this.mAuth = mAuth;
    }

    public void cargarRecyclerView(final RecyclerView mRecyclerView) {


        mDatabase.child("Buses").child("Casa").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<BusModel> arrayListBuses = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    BusModel busModel = snapshot.getValue(BusModel.class);
                    busModel.setHora((busModel.getHora()));
                    busModel.setCodigo((busModel.getCodigo()));
                    int num_asientos=busModel.getAsientos();
                    busModel.setAsientos(num_asientos);
                    busModel.setPlaca((busModel.getPlaca()));

                    arrayListBuses.add(busModel);

                }

                mAdapter = new RecyclerBusAdapter(mCOntext, R.layout.bus_row, arrayListBuses);
                mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void cargarRecyclerViewUniversidad(final RecyclerView mRecyclerView) {


        mDatabase.child("Buses").child("Universidad").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<BusModel> arrayListBuses = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    BusModel busModel = snapshot.getValue(BusModel.class);
                    busModel.setHora((busModel.getHora()));
                    busModel.setCodigo((busModel.getCodigo()));
                    int num_asientos=busModel.getAsientos();
                    busModel.setAsientos(num_asientos);
                    busModel.setPlaca((busModel.getPlaca()));

                    arrayListBuses.add(busModel);

                }

                mAdapter = new RecyclerBusAdapter(mCOntext, R.layout.bus_row, arrayListBuses);
                mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    public void cargaBusesFirebase(String hora, String codigo, int asientos, String placa, final Dialog dialog, final ProgressDialog progressDialog) {

        Map<String, Object> busCasa = new HashMap<>();
        busCasa.put("hora", hora);
        busCasa.put("codigo", codigo);
        busCasa.put("asientos", asientos);
        busCasa.put("placa", placa);
        mDatabase.child("Buses").child("Casa").push().updateChildren(busCasa).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                dialog.dismiss();
                progressDialog.dismiss();
                Toast.makeText(mCOntext, "Datos cargados correctamente", Toast.LENGTH_LONG).show();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                Toast.makeText(mCOntext, "Error al subir los datos" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void cargaBusesFirebaseUniversidad(String hora, String codigo, int asientos, String placa, final Dialog dialog, final ProgressDialog progressDialog) {

        Map<String, Object> busUni = new HashMap<>();
        busUni.put("hora", hora);
        busUni.put("codigo", codigo);
        busUni.put("asientos", asientos);
        busUni.put("placa", placa);
        mDatabase.child("Buses").child("Universidad").push().updateChildren(busUni).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                dialog.dismiss();
                progressDialog.dismiss();
                Toast.makeText(mCOntext, "Datos cargados correctamente", Toast.LENGTH_LONG).show();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                Toast.makeText(mCOntext, "Error al subir los datos" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


}
