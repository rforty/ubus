package facci.ronny_forty.ubus.Presentador.LoginPresenter;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import facci.ronny_forty.ubus.Vista.AdminView.Horarios_Casa;
import facci.ronny_forty.ubus.Vista.AdminView.MenuAdmin;
import facci.ronny_forty.ubus.Vista.MenuView.MainActivity;


public class PresentadorLogin {

    private Context mContext;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private String TAG = "Presentador Login";


    public PresentadorLogin(Context mContext, FirebaseAuth mAuth, DatabaseReference mDatabase) {
        this.mContext = mContext;
        this.mAuth = mAuth;
        this.mDatabase = mDatabase;
    }

    public void signInUser(String email, String password){

        final ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Ingresando...");
        dialog.setCancelable(false);
        dialog.show();
        mAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "signInWithEmail:success");
                            dialog.dismiss();
                            //  mDatabase.child("Usuarios").child(task.getResult().getUser().getUid()).child("titulo").setValue("FirebaseMVP");
                            Intent intent =  new Intent (mContext, MainActivity.class);
                            mContext.startActivity(intent);

                        }else {
                            dialog.dismiss();
                            Log.w(TAG,"signInWithEmail:failure", task.getException());
                            Toast.makeText(mContext, "Autenticación fallida.",Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }

    public void signInAdmin(final String email, String password){

        final ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Ingresando...");
        dialog.setCancelable(false);
        dialog.show();
        mAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "signInWithEmail:success");
                            dialog.dismiss();
                            // mDatabase.child("Usuarios").child(task.getResult().getUser().getUid()).child("titulo").setValue("FirebaseMVP");
                            Intent intent =  new Intent (mContext, MenuAdmin.class);
                            mContext.startActivity(intent);

                        }else {
                            dialog.dismiss();
                            Log.w(TAG,"signInWithEmail:failure", task.getException());
                            Toast.makeText(mContext, "Autenticación fallida.",Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }

    public void ComprobarCamposllenos(String email, String password){
        if(TextUtils.isEmpty(email)){
            Toast.makeText(mContext,"Debe ingresar un email",Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(password)){
            Toast.makeText(mContext,"Debe ingresar una contraseña",Toast.LENGTH_LONG).show();
            return;
        }else{
            signInUser(email,password);
        }

    }

    public void ComprobarCamposllenosAdmin(String email, String password){
        //validar email de administrador
        Pattern patronEmail = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[ubus.com-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matherEmail = patronEmail.matcher(email);

        if(TextUtils.isEmpty(email)){
            Toast.makeText(mContext,"Debe ingresar un email",Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(password)){
            Toast.makeText(mContext,"Debe ingresar una contraseña",Toast.LENGTH_LONG).show();
            return;
        }else{
            if (matherEmail.find() == false) {
                Toast.makeText(mContext,"Correo invalido, solo se permite Administrador ",Toast.LENGTH_SHORT).show();
            }else{
                signInAdmin(email,password);
            }


        }

    }


}


