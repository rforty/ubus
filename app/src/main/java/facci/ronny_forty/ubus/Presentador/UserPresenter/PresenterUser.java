package facci.ronny_forty.ubus.Presentador.UserPresenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import facci.ronny_forty.ubus.Adaptadores.RecyclerBusAdapterUser;
import facci.ronny_forty.ubus.Modelo.BusModel;
import facci.ronny_forty.ubus.R;

public class PresenterUser {

    private Context mCOntext;
    private DatabaseReference mDatabase;
    private FirebaseAuth  mAuth;
    private RecyclerBusAdapterUser mAdapter;


    public PresenterUser(Context mCOntext, DatabaseReference mDatabase, FirebaseAuth mAuth) {
        this.mCOntext = mCOntext;
        this.mDatabase = mDatabase;
        this.mAuth = mAuth;
    }



    public void cargarRecyclerViewUserCasa(final RecyclerView mRecyclerView ){


        mDatabase.child("Buses").child("Casa").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<BusModel> arrayListBusesUser = new ArrayList<>();

                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    BusModel busModel = snapshot.getValue(BusModel.class);
                    int num_asientos=busModel.getAsientos();
                    busModel.setHora(busModel.getHora());
                    busModel.setAsientos(num_asientos);

                    arrayListBusesUser.add(busModel);

                }

                mAdapter = new RecyclerBusAdapterUser(mCOntext, R.layout.bus_row_user,arrayListBusesUser);
                mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void cargarRecyclerViewUserUniversidad(final RecyclerView mRecyclerView ){


        mDatabase.child("Buses").child("Universidad").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<BusModel> arrayListBusesUser = new ArrayList<>();

                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    BusModel busModel = snapshot.getValue(BusModel.class);
                    int num_asientos=busModel.getAsientos();
                    busModel.setHora(busModel.getHora());
                    busModel.setAsientos(num_asientos);

                    arrayListBusesUser.add(busModel);

                }

                mAdapter = new RecyclerBusAdapterUser(mCOntext, R.layout.bus_row_user,arrayListBusesUser);
                mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }





}
