package facci.ronny_forty.ubus.Vista.AdminView;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import facci.ronny_forty.ubus.R;

public class MenuAdmin extends AppCompatActivity implements View.OnClickListener {

    Button btn_manta;
    Button btn_montecristi;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_admin);

        Toast.makeText(this,"Bienvenido Administrador", Toast.LENGTH_LONG).show();


        btn_manta = findViewById(R.id.btn_casa_admin);
        btn_manta.setOnClickListener(this);

        btn_montecristi = findViewById(R.id.btn_universidad_admin);
        btn_montecristi.setOnClickListener(this);





    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){

            case R.id.btn_casa_admin : Intent intent = new Intent(MenuAdmin.this, Horarios_Casa.class);

                startActivity(intent);

                break;

            case R.id.btn_universidad_admin : Intent intent2 = new Intent(MenuAdmin.this, Horarios_Universidad.class);
                startActivity(intent2);
                break;

        }

    }
}
